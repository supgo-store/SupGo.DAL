﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SupGo.DAL.NetCore.Extensions
{
    /// <summary>
    /// Extension class for List type
    /// </summary>
    public static class IdentifiersExtension
    {
        /// <summary>
        /// Remove the value of the id on a set of objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entities"></param>
        /// <returns></returns>
        public static IEnumerable<T> CleanIdentifiers<T>(this IEnumerable<T> entities) where T : class
        {
            entities = entities.ToList();
            foreach(T entity in entities)
            {
                Type type = entity.GetType();
                PropertyInfo pi = type.GetProperty("Id" + type.Name);
                pi.SetValue(entity, 0);
            }
            return entities;
        }

        /// <summary>
        /// Remove the value of the id
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static T CleanIdentifier<T>(this T entity) where T : class
        {
            if(entity != null)
            {
                Type type = entity.GetType();
                PropertyInfo pi = type.GetProperty("Id" + type.Name);
                pi.SetValue(entity, 0);
            }
            return entity;
        }
    }
}
