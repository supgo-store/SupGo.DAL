﻿using Microsoft.EntityFrameworkCore.Internal;
using SupGo.DAL.NetCore.Models;
using SupGo.DAL.NetCore.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SupGo.DAL.NetCore.Extensions
{
    public static class EagerLoadingExtension
    {
        public static IEnumerable<T> LoadChilds<T>(this IEnumerable<T> entities) where T : class
        {
            if (entities.Any())
            {
                entities = entities.ToList();
                Type type = entities.First().GetType();
                List<PropertyInfo> childProperties = type.GetProperties().ToList().FindAll(pi => pi.Name.StartsWith("Child"));
                List<PropertyInfo> uuidProperties = type.GetProperties().ToList().FindAll(pi => pi.Name.EndsWith("Uuid") && !pi.Name.Equals("Uuid"));

                if (childProperties.Any() && uuidProperties.Any())
                {
                    foreach(T entity in entities)
                    {
                        foreach(PropertyInfo pi in childProperties)
                        {
                            Type childType = pi.PropertyType;
                            string childUuid = (string) uuidProperties.Find(uuidPi => uuidPi.Name.StartsWith(childType.Name)).GetValue(entity);
                            PropertyInfo childUuidProperty = childType.GetProperty("Uuid");

                            object childObject = GetChildObject(childType.Name, childUuidProperty, childUuid);
                            pi.SetValue(entity, childObject);
                        }
                    }
                }
            }
            return entities;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static T LoadChilds<T>(this T entity) where T : class
        {
            if (entity != null)
            {
                Type type = entity.GetType();
                List<PropertyInfo> childProperties = type.GetProperties().ToList().FindAll(pi => pi.Name.StartsWith("Child"));
                List<PropertyInfo> uuidProperties = type.GetProperties().ToList().FindAll(pi => pi.Name.EndsWith("Uuid") && !pi.Name.Equals("Uuid"));

                if (childProperties.Any() && uuidProperties.Any())
                {
                    foreach (PropertyInfo pi in childProperties)
                    {
                        Type childType = pi.PropertyType;
                        string childUuid = (string)uuidProperties.Find(uuidPi => uuidPi.Name.StartsWith(childType.Name)).GetValue(entity);
                        PropertyInfo childUuidProperty = childType.GetProperty("Uuid");

                        object childObject = GetChildObject(childType.Name, childUuidProperty, childUuid);
                        pi.SetValue(entity, childObject);
                    }
                }
            } 
            return entity;
        }

        /// <summary>
        /// Obligé de faire un truc dégueulasse comme ça parce-que je suis limité par le niveau de généricité possible ='C
        /// </summary>
        /// <param name="type"></param>
        /// <param name="childUuidProperty"></param>
        /// <param name="childUuid"></param>
        /// <returns></returns>
        private static object GetChildObject(string type, PropertyInfo childUuidProperty, string childUuid)
        {
            switch (type)
            {
                case "Action":
                    return DatabaseService<Models.Action>.GetSingle(true, a => a.Uuid.Equals(childUuid));
                case "Cart":
                    return DatabaseService<Cart>.GetSingle(true, c => c.Uuid.Equals(childUuid));
                case "Category":
                    return DatabaseService<Category>.GetSingle(true, c => c.Uuid.Equals(childUuid));
                case "Comment":
                    return DatabaseService<Comment>.GetSingle(true, c => c.Uuid.Equals(childUuid));
                case "Discount":
                    return DatabaseService<Discount>.GetSingle(true, d => d.Uuid.Equals(childUuid));
                case "History":
                    return DatabaseService<History>.GetSingle(true, h => h.Uuid.Equals(childUuid));
                case "Orders":
                    return DatabaseService<Orders>.GetSingle(true, o => o.Uuid.Equals(childUuid));
                case "Product":
                    return DatabaseService<Product>.GetSingle(true, p => p.Uuid.Equals(childUuid));
                case "Stock":
                    return DatabaseService<Stock>.GetSingle(true, s => s.Uuid.Equals(childUuid));
                case "Store":
                    return DatabaseService<Store>.GetSingle(true, s => s.Uuid.Equals(childUuid));
                case "User":
                    return DatabaseService<User>.GetSingle(true, u => u.Uuid.Equals(childUuid));
                default:
                    return new object();
            }
        }
    }
}
