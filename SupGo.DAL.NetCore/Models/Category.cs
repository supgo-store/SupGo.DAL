﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Category
    {
        public Category()
        {
            Product = new HashSet<Product>();
        }

        public int IdCategory { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string Department { get; set; }

        public ICollection<Product> Product { get; set; }
    }
}
