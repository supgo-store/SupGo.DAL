﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Store
    {
        public Store()
        {
            Cart = new HashSet<Cart>();
            History = new HashSet<History>();
            Stock = new HashSet<Stock>();
        }

        public int IdStore { get; set; }
        public string Uuid { get; set; }
        public string Name { get; set; }
        public string Latitude { get; set; }
        public string Longitude { get; set; }

        public ICollection<Cart> Cart { get; set; }
        public ICollection<History> History { get; set; }
        public ICollection<Stock> Stock { get; set; }
    }
}
