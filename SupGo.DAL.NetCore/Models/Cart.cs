﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Cart
    {
        public Cart()
        {
            Orders = new HashSet<Orders>();
        }

        public int IdCart { get; set; }
        public string Uuid { get; set; }
        public string UserUuid { get; set; }
        public string StoreUuid { get; set; }
        public DateTime? Datetime { get; set; }
        public int? TotalPrice { get; set; }

        public Store ChildStore { get; set; }
        public User ChildUser { get; set; }
        public ICollection<Orders> Orders { get; set; }
    }
}
