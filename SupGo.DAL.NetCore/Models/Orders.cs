﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Orders
    {
        public int IdOrders { get; set; }
        public string Uuid { get; set; }
        public int Quantity { get; set; }
        public string CartUuid { get; set; }
        public string ProductUuid { get; set; }
        public string DiscountUuid { get; set; }

        public Cart ChildCart { get; set; }
        public Discount ChildDiscount { get; set; }
        public Product ChildProduct { get; set; }
    }
}
