﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class User
    {
        public User()
        {
            Cart = new HashSet<Cart>();
            Comment = new HashSet<Comment>();
            History = new HashSet<History>();
        }

        public int IdUser { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Uuid { get; set; }
        public string CardNumber { get; set; }
        public DateTime? CardExpireDate { get; set; }
        public string CardOwner { get; set; }
        public int? CardCcv { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public DateTime? Birthdate { get; set; }
        public string Gender { get; set; }

        public ICollection<Cart> Cart { get; set; }
        public ICollection<Comment> Comment { get; set; }
        public ICollection<History> History { get; set; }
    }
}
