﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class History
    {
        public int IdHistory { get; set; }
        public string Uuid { get; set; }
        public DateTime Datetime { get; set; }
        public string ActionUuid { get; set; }
        public string ProductUuid { get; set; }
        public string UserUuid { get; set; }
        public string StoreUuid { get; set; }

        public Action ChildAction { get; set; }
        public Product ChildProduct { get; set; }
        public Store ChildStore { get; set; }
        public User ChildUser { get; set; }
    }
}
