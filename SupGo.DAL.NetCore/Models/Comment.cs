﻿using System;
using System.Collections.Generic;

namespace SupGo.DAL.NetCore.Models
{
    public partial class Comment
    {
        public int IdComment { get; set; }
        public string Uuid { get; set; }
        public DateTime Datetime { get; set; }
        public int NbStars { get; set; }
        public string Text { get; set; }
        public string ProductUuid { get; set; }
        public string UserUuid { get; set; }

        public Product ChildProduct { get; set; }
        public User ChildUser { get; set; }
    }
}
