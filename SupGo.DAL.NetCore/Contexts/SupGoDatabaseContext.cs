﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using SupGo.DAL.NetCore.Models;
using Action = SupGo.DAL.NetCore.Models.Action;

namespace SupGo.DAL.NetCore.Contexts
{
    public partial class SupGoDatabaseContext : DbContext
    {
        public SupGoDatabaseContext()
        {
        }

        public SupGoDatabaseContext(DbContextOptions<SupGoDatabaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Action> Action { get; set; }
        public virtual DbSet<Cart> Cart { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<Comment> Comment { get; set; }
        public virtual DbSet<Discount> Discount { get; set; }
        public virtual DbSet<History> History { get; set; }
        public virtual DbSet<Orders> Orders { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<Stock> Stock { get; set; }
        public virtual DbSet<Store> Store { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql("server=51.158.97.34;database=supgo_database;user=root;pwd=supgo_password;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Action>(entity =>
            {
                entity.HasKey(e => e.IdAction);

                entity.ToTable("action");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdAction).HasColumnName("idAction");

                entity.Property(e => e.ActionType)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<Cart>(entity =>
            {
                entity.HasKey(e => e.IdCart);

                entity.ToTable("cart");

                entity.HasIndex(e => e.StoreUuid)
                    .HasName("StoreUUID");

                entity.HasIndex(e => e.UserUuid)
                    .HasName("UserUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdCart).HasColumnName("idCart");

                entity.Property(e => e.Datetime).HasColumnType("datetime");

                entity.Property(e => e.StoreUuid)
                    .IsRequired()
                    .HasColumnName("StoreUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserUuid)
                    .IsRequired()
                    .HasColumnName("UserUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildStore)
                    .WithMany(p => p.Cart)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.StoreUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_ibfk_2");

                entity.HasOne(d => d.ChildUser)
                    .WithMany(p => p.Cart)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.UserUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("cart_ibfk_1");
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.HasKey(e => e.IdCategory);

                entity.ToTable("category");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdCategory).HasColumnName("idCategory");

                entity.Property(e => e.Department)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<Comment>(entity =>
            {
                entity.HasKey(e => e.IdComment);

                entity.ToTable("comment");

                entity.HasIndex(e => e.ProductUuid)
                    .HasName("ProductUUID");

                entity.HasIndex(e => e.UserUuid)
                    .HasName("UserUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdComment).HasColumnName("idComment");

                entity.Property(e => e.Datetime)
                    .HasColumnType("datetime")
                    .HasDefaultValueSql("'CURRENT_TIMESTAMP'");

                entity.Property(e => e.NbStars).HasColumnName("nbStars");

                entity.Property(e => e.ProductUuid)
                    .IsRequired()
                    .HasColumnName("ProductUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Text)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.UserUuid)
                    .IsRequired()
                    .HasColumnName("UserUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildProduct)
                    .WithMany(p => p.Comment)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.ProductUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("comment_ibfk_1");

                entity.HasOne(d => d.ChildUser)
                    .WithMany(p => p.Comment)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.UserUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("comment_ibfk_2");
            });

            modelBuilder.Entity<Discount>(entity =>
            {
                entity.HasKey(e => e.IdDiscount);

                entity.ToTable("discount");

                entity.HasIndex(e => e.ProductUuid)
                    .HasName("ProductUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdDiscount).HasColumnName("idDiscount");

                entity.Property(e => e.EndDate).HasColumnType("datetime");

                entity.Property(e => e.ProductUuid)
                    .IsRequired()
                    .HasColumnName("ProductUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.StartDate).HasColumnType("datetime");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildProduct)
                    .WithMany(p => p.Discount)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.ProductUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("discount_ibfk_1");
            });

            modelBuilder.Entity<History>(entity =>
            {
                entity.HasKey(e => e.IdHistory);

                entity.ToTable("history");

                entity.HasIndex(e => e.ActionUuid)
                    .HasName("ActionUUID");

                entity.HasIndex(e => e.ProductUuid)
                    .HasName("ProductUUID");

                entity.HasIndex(e => e.StoreUuid)
                    .HasName("StoreUUID");

                entity.HasIndex(e => e.UserUuid)
                    .HasName("UserUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdHistory).HasColumnName("idHistory");

                entity.Property(e => e.ActionUuid)
                    .IsRequired()
                    .HasColumnName("ActionUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Datetime).HasColumnType("datetime");

                entity.Property(e => e.ProductUuid)
                    .IsRequired()
                    .HasColumnName("ProductUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.StoreUuid)
                    .IsRequired()
                    .HasColumnName("StoreUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.UserUuid)
                    .IsRequired()
                    .HasColumnName("UserUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildAction)
                    .WithMany(p => p.History)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.ActionUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("history_ibfk_1");

                entity.HasOne(d => d.ChildProduct)
                    .WithMany(p => p.History)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.ProductUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("history_ibfk_2");

                entity.HasOne(d => d.ChildStore)
                    .WithMany(p => p.History)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.StoreUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("history_ibfk_4");

                entity.HasOne(d => d.ChildUser)
                    .WithMany(p => p.History)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.UserUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("history_ibfk_3");
            });

            modelBuilder.Entity<Orders>(entity =>
            {
                entity.HasKey(e => e.IdOrders);

                entity.ToTable("orders");

                entity.HasIndex(e => e.CartUuid)
                    .HasName("CartUUID");

                entity.HasIndex(e => e.DiscountUuid)
                    .HasName("DiscountUUID");

                entity.HasIndex(e => e.ProductUuid)
                    .HasName("ProductUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdOrders).HasColumnName("idOrder");

                entity.Property(e => e.CartUuid)
                    .IsRequired()
                    .HasColumnName("CartUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.DiscountUuid)
                    .HasColumnName("DiscountUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.ProductUuid)
                    .IsRequired()
                    .HasColumnName("ProductUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildCart)
                    .WithMany(p => p.Orders)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.CartUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_ibfk_1");

                entity.HasOne(d => d.ChildDiscount)
                    .WithMany(p => p.Orders)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.DiscountUuid)
                    .HasConstraintName("orders_ibfk_3");

                entity.HasOne(d => d.ChildProduct)
                    .WithMany(p => p.Orders)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.ProductUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("orders_ibfk_2");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.HasKey(e => e.IdProduct);

                entity.ToTable("product");

                entity.HasIndex(e => e.CategoryUuid)
                    .HasName("CategoryUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdProduct).HasColumnName("idProduct");

                entity.Property(e => e.CategoryUuid)
                    .IsRequired()
                    .HasColumnName("CategoryUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnType("text");

                entity.Property(e => e.Image)
                    .IsRequired()
                    .HasColumnType("longtext");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildCategory)
                    .WithMany(p => p.Product)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.CategoryUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("product_ibfk_1");
            });

            modelBuilder.Entity<Stock>(entity =>
            {
                entity.HasKey(e => e.IdStock);

                entity.ToTable("stock");

                entity.HasIndex(e => e.ProductUuid)
                    .HasName("ProductUUID");

                entity.HasIndex(e => e.StoreUuid)
                    .HasName("StoreUUID");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdStock).HasColumnName("idStock");

                entity.Property(e => e.ProductUuid)
                    .IsRequired()
                    .HasColumnName("ProductUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.StoreUuid)
                    .IsRequired()
                    .HasColumnName("StoreUUID")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");

                entity.HasOne(d => d.ChildProduct)
                    .WithMany(p => p.Stock)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.ProductUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stock_ibfk_1");

                entity.HasOne(d => d.ChildStore)
                    .WithMany(p => p.Stock)
                    .HasPrincipalKey(p => p.Uuid)
                    .HasForeignKey(d => d.StoreUuid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("stock_ibfk_2");
            });

            modelBuilder.Entity<Store>(entity =>
            {
                entity.HasKey(e => e.IdStore);

                entity.ToTable("store");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdStore).HasColumnName("idStore");

                entity.Property(e => e.Latitude)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Longitude)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.HasKey(e => e.IdUser);

                entity.ToTable("user");

                entity.HasIndex(e => e.Uuid)
                    .HasName("UUID")
                    .IsUnique();

                entity.Property(e => e.IdUser).HasColumnName("idUser");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Birthdate)
                    .HasColumnName("birthdate")
                    .HasColumnType("date");

                entity.Property(e => e.CardCcv).HasColumnName("cardCCV");

                entity.Property(e => e.CardExpireDate)
                    .HasColumnName("cardExpireDate")
                    .HasColumnType("date");

                entity.Property(e => e.CardNumber)
                    .HasColumnName("cardNumber")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.CardOwner)
                    .HasColumnName("cardOwner")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.City)
                    .HasColumnName("city")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Firstname)
                    .IsRequired()
                    .HasColumnName("firstname")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Gender)
                    .HasColumnName("gender")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.Lastname)
                    .IsRequired()
                    .HasColumnName("lastname")
                    .HasColumnType("varchar(255)");

                entity.Property(e => e.Phone)
                    .HasColumnName("phone")
                    .HasColumnType("varchar(20)");

                entity.Property(e => e.PostalCode)
                    .HasColumnName("postalCode")
                    .HasColumnType("varchar(10)");

                entity.Property(e => e.Uuid)
                    .IsRequired()
                    .HasColumnName("UUID")
                    .HasColumnType("varchar(255)");
            });
        }
    }
}
